<?php

/**
 * @file
 * Plugin include file for .arff export style plugin.
 */

/**
 * Style plugin for exporting .arff files.
 *
 * @ingroup views_style_plugins
 */
class views_arff_plugin_style extends views_data_export_plugin_style_export {
  var $feed_text = 'ARFF';
  var $feed_file = '%view.arff';

  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options = NULL);
    $this->feed_image = drupal_get_path('module', 'views_arff') . '/weka.png';
    $this->feed_file = '%view.arff';
    $this->feed_text = 'ARFF';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['relation'] = array(
      'default' => '',
      'translatable' => TRUE,
    );
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['relation'] = array(
      '#type' => 'textfield',
      '#title' => t('Relation'),
      '#default_value' => $this->options['relation'],
      '#description' => t('Used for the @relation declaration.'),
    );
  }

}
