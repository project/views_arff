<?php

/**
 * @file
 * Views include file with views hooks.
 */

/**
 * Implementation of hook_views_plugins().
 */
function views_arff_views_plugins() {
  $path = drupal_get_path('module', 'views_arff');
  return array(
    'style' => array(
      'views_arff' => array(
        'title' => t('ARFF file'),
        'help' => t("Export view in Weka's ARFF format."),
        'path' => $path,
        'handler' => 'views_arff_plugin_style',
        'parent' => 'views_data_export',
        'theme' => 'views_data_export',
        'theme path' => $path . '/theme',
        'theme file' => 'views_arff.theme.inc',
        // Views Data Export element that will be used to set additional headers
        // when serving the feed.
        'export headers' => array('Content-type: text/plain; charset=utf-8'),
        // Views Data Export element mostly used for creating some additional
        // classes and template names.
        'export feed type' => 'arff',
        'additional themes' => array(
          'views_arff_header' => 'style',
          'views_arff_footer' => 'style',
          'views_arff_body' => 'style',
        ),
        'additional themes base' => 'views_arff',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'data_export',
      ),
    ),
  );
}
