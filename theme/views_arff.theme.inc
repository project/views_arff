<?php

/**
 * @file
 * Theme functions.
 */

// Include views data export theme functions.
module_load_include('inc', 'views_data_export', 'theme/views_data_export.theme');

function template_preprocess_views_arff_header(&$vars) {
  _views_data_export_header_shared_preprocess($vars);

  // Add relation declaration.
  $vars['relation'] = trim(check_plain($vars['options']['relation']));
  // If there's space, add quotes to it.
  $vars['relation'] = (strpos($vars['relation'], ' ') === FALSE) ? $vars['relation'] : '"' . $vars['relation'] . '"';

  // Format header values.
  foreach ($vars['header'] as $key => $value) {
    $field = $vars['view']->field[$key];
    $attribute_type = views_arff_get_type($field);
    // Handle nominal fields.
    if (module_exists('content') && $attribute_type == VIEWS_ARFF_STRING && isset($field->content_field)) {
      $allowed_values = content_allowed_values($field->content_field);
      if (!empty($allowed_values)) {
        // Sanitize and add quotes when needed.
        $allowed_values = array_map('views_arff_sanitize_string', $allowed_values);
        $attribute_type = '{' . implode(',', $allowed_values) . '}';
      }
    }

    $vars['header'][$key] = (object) array(
      'type' => $attribute_type,
      'label' => views_arff_sanitize_string($value),
    );    
  }
}

function template_preprocess_views_arff_body(&$vars) {
  _views_data_export_body_shared_preprocess($vars);
  // Format row values.
  foreach ($vars['themed_rows'] as $i => $values) {
    foreach ($values as $j => $value) {
      //$vars['themed_rows'][$i][$j] = $wrap . str_replace('"', $replace_value, $output) . $wrap;
      if (!empty($value)) {
        $vars['themed_rows'][$i][$j] = is_string($value) ? views_arff_sanitize_string($value) : $value;
      }
      else {
        $vars['themed_rows'][$i][$j] = '?';
      }
    }
  }
}
